const { FuseBox, WebIndexPlugin } = require('fuse-box');

const fuse = FuseBox.init({
  homeDir: 'src',
  target: 'browser@es6',
  output: 'dist/$name.js',
  plugins: [WebIndexPlugin()],
  useTypeScriptCompiler: true,
  sourceMaps: true,
});

fuse.dev();
fuse
  .bundle('app')
  .instructions(' > index.ts')
  .hmr()
  .watch();
fuse.run();
